package net.techu;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
@RestController
public class ProductosController {
    private ArrayList<String> listaProductos = null;
    public ProductosController() {
        listaProductos = new ArrayList<>();
        listaProductos.add("PR1");
        listaProductos.add("PR2");
    }
    /* Get lista de productos */
    @GetMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<List<String>> obtenerListado()
    {
        System.out.println("Estoy en obtener");
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }
    @GetMapping("/productos/{id}")
    public ResponseEntity<String> obtenerProductoPorId(@PathVariable int id)
    {
        String resultado = null;
        ResponseEntity<String> respuesta = null;
        try
        {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            resultado = "No se ha encontrado el producto";
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }
    /* Add nuevo producto */
    @PostMapping(value = "/productos", produces="application/json")
    public void addProducto() {
        System.out.println("Estoy en añadir");
        listaProductos.add("NUEVO");
    }
    /* Add nuevo producto con nombre */
    @PostMapping(value = "/productos/{nom}/{cat}", produces="application/json")
    public ResponseEntity<String> addProductoConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria) {
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        listaProductos.add(nombre);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }
    @PutMapping("/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            String productoAModificar = listaProductos.get(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
    @DeleteMapping("/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            String productoAEliminar = listaProductos.get(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
}