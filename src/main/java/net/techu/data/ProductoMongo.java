package net.techu.data;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("productosangelrayo")
public class ProductoMongo {

    public String id;
    public String nombre;
    public Double precio;
    public String color;

    public ProductoMongo() {
    }

    public ProductoMongo(String nombre, Double precio, String color) {
        this.nombre = nombre;
        this.precio = precio;
        this.color = color;
    }

    @Override
    public String toString() {
        return String.format("Producto [id=%s, nombre=%s, precio=%s, color=%s]", id, nombre, precio, color);
    }

}
